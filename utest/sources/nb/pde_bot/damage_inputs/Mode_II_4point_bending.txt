#################################################
# Victor Eduardo Cardoso Nungaray		#
# 2010-2016					#
# Three point bending test setup                #
#################################################


#-------------- Geometry definition ------------#
# Number of vertices				#
 9						#
# Vertices coordinates                          #
   0.0   0.0					#
 222.5   0.0					#
 222.5  50.0					#
 227.5  50.0					#
 227.5   0.0					#
 450.0   0.0					#
 450.0 100.0					#
 225.0 100.0					#
   0.0 100.0					#
						#
# Number of segments				#
 9						#
# Segments (Vertices pairs IDs)			#
 0 1    					#
 1 2						#
 2 3						#
 3 4						#
 4 5                                            #
 5 6						#
 6 7						#
 7 8						#
 8 0						#
# Number of holes				#
 0						#
#-----------------------------------------------#

#-------------- Boundary conditions ------------#
# Num displacement conditions over vertices:    #
 2						#
 0 1 1 0.0 0.0                                  #
 5 0 1 0.0                                      #
# Num of load conditions over vertices:		#
 1						#
 7 0 1 1e-3                                     #
# Num of displacement conditions over lines:    #
 0						#
# Num of load conditions over lines:            #
 0						#
#-----------------------------------------------#

#-------------- Material properties ------------#
# Poisson module				#
 0.2						#
# Elasticity module				#
 2e10	                                        #
# Fracture energy                               #
 113						#
# Compression limit stress                      #
 0						#
# Traction limit stress                         #
 0						#
# Tensile stength 2.4e6
#-----------------------------------------------#

#---------------- Analysis ---------------------#
#--- Does the analysis uses plane stress?-------#
# (0: Plane stress, 1: Plane strain, 2: S.Rev)	#
 1						#
# Analysis params (Thickness axis of rev)       #
 0.1	                                        #
#-----------------------------------------------#
