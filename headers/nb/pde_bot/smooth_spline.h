#ifndef __NB_PDE_BOT_SMOOTH_SPLINE_H__
#define __NB_PDE_BOT_SMOOTH_SPLINE_H__

#include <stdint.h>

double nb_smooth_spline(double x, uint8_t smooth);

#endif
