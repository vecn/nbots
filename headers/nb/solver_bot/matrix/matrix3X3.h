#ifndef __NB_SOLVER_BOT_MATRIX_MATRIX3X3_H__
#define __NB_SOLVER_BOT_MATRIX_MATRIX3X3_H__

double nb_matrix_3X3_det(double *A);

double nb_matrix_3X3_inverse_destructive(double *A);

#endif
